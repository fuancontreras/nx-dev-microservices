# nx-dev-microservices

Getting Udemy Microservices project worwing with skaffold and nx.dev architecture

# 1. Setting microservices with shared library
Using `yalk` to create the local installation of the package.
```sh
# Install the package
npm install -g yalc
```

## Go to the library file
In this case `common` and publish it locally
```
yalc publish
```
This will create a local centralized copy managed by yalc.
## Go to the other apps
In the other apps use
```sh
# First time
yalc add @js-libs/common
# If you want to update
yalc update @js-libs/common
```
> Dockerfiles need to be updated (Yalc doesn't need to be installed in the docekr image) as long as the `.yalc` folder is copied and the `yalc.lock` folder is available before running `npm i` then everything should work as expected.
