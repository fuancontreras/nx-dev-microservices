require("express-async-errors");
import mongoose from "mongoose";
import { app } from "./app";


// mongoose start function
const start = async () => {
  // Verify environment variables.
  if (!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must eb defined');
  }
  if (!process.env.MONGO_URI) {
    throw new Error('MONGO_URI must eb defined');
  }
  try {
    await mongoose.connect(process.env.MONGO_URI!, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log('Connected to Mongo DB');
  } catch (err) {
    console.error(err);
  }
};

// Init server
app.listen(3000, () => {
  console.log("Listening on port 3000 (auth)");
});

// Call mongoose connection
start();