import express from "express";
require("express-async-errors");

import { json } from "body-parser";
import { errorHandler, NotFoundError } from "@js-libs/common";
import Cookiesession from 'cookie-session';

// Import routes
import { currentUserRouter } from "./routes/current-user";
import { signInRouter } from "./routes/signin";
import { signOutRouter } from "./routes/signout";
import { signUpRouter } from "./routes/signup";

// create App
const app = express();
// add body parser
app.use(json());
// Set trust certificate certificatge.
app.set('trust proxy', true);

// Add cookie manager middleware
app.use(
  Cookiesession({
    // Disabling encryption
    signed: false,
    // Require https (Supertest not supported)
    secure: process.env.NODE_ENV !== 'test',
  })
);

// Apply route (register route)
app.use(signUpRouter);
app.use(currentUserRouter);
app.use(signInRouter);
app.use(signOutRouter);

/** Default route */
app.all("*", async () => {
  throw new NotFoundError();
});

// Applyerror handler
app.use(errorHandler);

// Exporting app to be used with SuperTest
export { app };