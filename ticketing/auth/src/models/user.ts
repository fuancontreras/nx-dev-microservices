import { Password } from "./../services/password";
import mongoose from "mongoose";

// An interface that desceribes the properties that ar required to create a new user
interface UserAtttrs {
  email: string;
  password: string;
}

// An interface to type the model
interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAtttrs): UserDoc;
}

// An Interface for the document itself
interface UserDoc extends mongoose.Document {
  email: string;
  password: string;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
}, {
  toJSON: {
    /**
     * A transform function to apply to the resulting document before returning
     * @param doc The mongoose document which is being converted
     * @param ret The plain object representation which has been converted
     * @param options The options in use (either schema options or the options passed inline)
     */
    transform(doc, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.password;
      delete ret.__v;
    }
  }
});

// Adding hashing using pre hook
userSchema.pre("save", async function (done) {
  // (this) is the docuemnt
  // modified is also on create
  if (this.isModified("password")) {
    const hashed = await Password.toHash(this.get("password"));
    this.set("password", hashed);
    done();
  }
});

// build and attach a static function to enforce typing
userSchema.statics.build = (attrs: UserAtttrs) => {
  return new User(attrs);
};

// Initialize model using generics for interface.
const User = mongoose.model<UserDoc, UserModel>("User", userSchema);

export { User };
