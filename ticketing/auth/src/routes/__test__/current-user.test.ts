import request from 'supertest';
import { app } from '../../app';

it('Responds wuth details about the current user', async () => {
  // Get a valid signup cookie
  const cookie = await global.signup();
  
  // Prepare current user request
  const response = await request(app)
  .get('/api/users/currentusers')
  // Set the cookie from the previous request
  .set('Cookie', cookie)
  .send()
  .expect(200);
  expect(response.body.currentUser.email).toEqual('test@test.com');
});

it ('Responds with an empty user when non-authenticated', async() => {
  const response = await request(app)
  .get('/api/users/currentusers')
  .send()
  .expect(200);
  expect(response.body.currentUser).toEqual(null);
});