import express, { Request, Response } from 'express';
import { currentUser } from '@js-libs/common';

// Creating router in a separate file NextJS
const router = express.Router();

router.get('/api/users/currentusers', [currentUser], (req: Request, res: Response) => {
  // As middleware injects the currentUser if exists
  res.send({ currentUser: req.currentUser || null })
});

export { router as currentUserRouter };