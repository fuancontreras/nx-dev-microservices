import { BadRequestError, validateRequest } from "@js-libs/common";
import express, { Request, Response } from "express";
import { body } from "express-validator";
// Importing JWT lib
import jwt from 'jsonwebtoken';

// error clases
import { User } from "../models/user";

const router = express.Router();

router.post(
  "/api/users/signup",
  [
    body("email").isEmail().withMessage("Email must be valid"),
    body("password")
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage("Password must be between 4 and 20 characters."),
      validateRequest
  ],
  async (req: Request, res: Response) => {
    // get Data
    const { email, password } = req.body;
    // Check if the email has been used
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new BadRequestError("Email already in use");
    }
    // Create new user
    const user = User.build({ email, password});
    // Save it to the Database
    await user.save();

    // Generate JWT
    const userJwt = jwt.sign({
      id: user.id,
      email: user.email
    }, process.env.JWT_KEY!);

    // Store it on session object
    // Cookie session is going to autmatically store it in the cookie.
    req.session= {
      jwt: userJwt
    }
    // Return standarized reusable way
    res.status(201).send(user);
  }
);

export { router as signUpRouter };
