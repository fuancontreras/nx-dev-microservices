import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import request from 'supertest';
import { app } from '../app';

declare global {
  namespace NodeJS {
    interface Global {
      signup(): Promise<string[]>
    }
  }
}

// Global helper function
global.signup = async () => {
  const email = 'test@test.com';
  const password = 'password';
  // Create an account
  const authReponse = await request(app)
    .post('/api/users/signup')
    .send({
      email,
      password
    })
    .expect(201);
  // Extract cookie from signup
  const cookie = authReponse.get('Set-Cookie');
  return cookie;
}

// encapsulate mongo var
let mongo: any;

// Start mongodb in memory server
beforeAll(async () => {
  process.env.JWT_KEY = 'asdfasdf';
  mongo = new MongoMemoryServer();
  const mongoUri = await mongo.getUri();
  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
});

beforeEach(async () => {
  // reset all data
  const collections = await mongoose.connection.db.collections();
  for (let collection of collections) {
    await collection.deleteMany({});
  }
});

afterAll(async () => {
  // Stop server an close connection
  await mongo.stop();
  mongoose.connection.close();
});