import { Publisher, Subjects, TicketUpdatedEvent} from '@js-libs/common';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  // Type and Value are the same when using ENUMs
  readonly subject: Subjects.TicketUpdated = Subjects.TicketUpdated;
}
