import { Publisher, Subjects, TicketCreatedEvent} from '@js-libs/common';

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
  // Type and Value are the same when using ENUMs
  readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
}
