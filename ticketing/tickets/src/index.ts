require("express-async-errors");
import mongoose from "mongoose";
import { app } from "./app";
import { natsWrapper } from "./nats-wrapper";


// mongoose start function
const start = async () => {
  // Verify environment variables.
  if (!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must be defined');
  }
  if (!process.env.MONGO_URI) {
    throw new Error('MONGO_URI must be defined');
  }
  if (!process.env.NATS_CLIENT_ID) {
    throw new Error('NATS_CLIENT_ID must be defined');
  }
  if (!process.env.NATS_URL) {
    throw new Error('NATS_URL must be defined');
  }
  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error('NATS_CLUSTER_ID must be defined');
  }
  try {
    // NATSS Connection
    // ticketing comes from the configuration paramters in the natts depl yaml
    await natsWrapper.connect(process.env.NATS_CLUSTER_ID, process.env.NATS_CLIENT_ID, process.env.NATS_URL)

    // Add graceful SHUTDOWN here
    natsWrapper.client.on('close', () => {
      console.log('NATS connection closed!');
      process.exit();
    });
    // Handler for console close events GRAECFUL PROCESS CLOSE!
    process.on('SIGINT', () => { natsWrapper.client.close() });
    process.on('SIGTERM', () => { natsWrapper.client.close() });

    // Mongoose Conenction
    await mongoose.connect(process.env.MONGO_URI!, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log('Connected to Mongo DB');
  } catch (err) {
    console.error(err);
  }
};

// Init server
app.listen(3000, () => {
  console.log("Listening on port 3000 (auth)");
});

// Call mongoose connection
start();