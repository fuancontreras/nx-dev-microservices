// Fake natsWrapper instance
export const natsWrapper = {
  client: {
    // Fake implemntation of publish
    publish: jest.fn().mockImplementation((subject: string, data: string, callback: () => void) => {
      callback();
    })
  }
}