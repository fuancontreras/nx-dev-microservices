import express from "express";
require("express-async-errors");

import { json } from "body-parser";
import { currentUser, errorHandler, NotFoundError } from "@js-libs/common";
import Cookiesession from 'cookie-session';

// Import routes
import { newTicketRouter } from "./routes/new";
import { showTicketRouter } from "./routes/show";
import { ticketsIndexRoute } from "./routes/index";
import { updateTicketRoute } from "./routes/update";

// create App
const app = express();
// add body parser
app.use(json());
// Set trust certificate certificatge.
app.set('trust proxy', true);

// Add cookie manager middleware
app.use(
  Cookiesession({
    // Disabling encryption
    signed: false,
    // Require https (Supertest not supported)
    secure: process.env.NODE_ENV !== 'test',
  })
);

// As all routes are authenticated use the currentUser middleware
app.use(currentUser);

// Apply route (register route)
app.use(newTicketRouter);
app.use(showTicketRouter);
app.use(ticketsIndexRoute);
app.use(updateTicketRoute);

/** Default route */
app.all("*", async () => {
  throw new NotFoundError();
});

// Applyerror handler
app.use(errorHandler);

// Exporting app to be used with SuperTest
export { app };