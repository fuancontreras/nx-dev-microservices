import mongoose from "mongoose";

// Model Attributes interface
interface TicketAttrs {
  title: string,
  price: number,
  userId: string
}

// Document Interface
interface TicketDoc extends mongoose.Document {
  title: string,
  price: number,
  userId: string
}

// Model Interface (here is why we need the document and attr interfaces)
interface TicketModel extends mongoose.Model<TicketDoc> {
  build(attrs: TicketAttrs): TicketDoc
}

// Creating the Model Schema (Using JS types!)
const ticketSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  userId: {
    type: String,
    required: true
  }
},
  {
    // in the options second paramter add JSON standarization
    toJSON: {
      transform(doc, ret) {
        // add new property id
        ret.id = ret._id
        // remove old property _id
        delete ret._id
      }
    }
  });

// Use an static constructor(factory) to being able to define types
ticketSchema.statics.build = (attr: TicketAttrs) => {
  return new Ticket(attr);
}

  // Create the model itself with the schema
const Ticket = mongoose.model<TicketDoc, TicketModel>('Ticket', ticketSchema);


export { Ticket };
