import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';

declare global {
  namespace NodeJS {
    interface Global {
      signup(uid?:string): string[]
    }
  }
}

// Global helper function
global.signup = (id:string='uid-798987678') => {
  // Build a JWT payload { id, email }
  const payload =  {
    id,
    email: 'test@test.com'
  };

  // create the JWT
  const token = jwt.sign(payload, process.env.JWT_KEY!);
  
  // Build Session object {jwt: MY_JWT}
  const session = {jwt: token};
  // Encode to JSON
  const sessionJSON = JSON.stringify(session);
  // Encode it as base64 (cookie session does it in the regula app)
  const base64Session = Buffer.from(sessionJSON).toString('base64');
  // Return encoded cookie
  return [`express:sess=${base64Session}`];
}

// encapsulate mongo var
let mongo: any;

// Start mongodb in memory server
beforeAll(async () => {
  process.env.JWT_KEY = 'asdfasdf';
  mongo = new MongoMemoryServer();
  const mongoUri = await mongo.getUri();
  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
});

beforeEach(async () => {
  // clear all mocks calls
  jest.clearAllMocks();
  // reset all data
  const collections = await mongoose.connection.db.collections();
  for (let collection of collections) {
    await collection.deleteMany({});
  }
});

afterAll(async () => {
  // Stop server an close connection
  await mongo.stop();
  mongoose.connection.close();
});