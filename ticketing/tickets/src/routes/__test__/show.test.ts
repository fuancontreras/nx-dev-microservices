import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

// Tell JEST to use FAKE wrapper (path to real file)
jest.mock('../../nats-wrapper');

it('Resturns 404 if the ticket is not found', async () => {
  const id = mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/tickets/${id}`)
    .send()
    .expect(404)
})

it('Resturns the ticket if the ticket is found', async () => {
  const cookie = await global.signup();
  // Create ticket
  const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
      title: 'My Test ticket',
      price: '100.00'
    });
  expect(response.status).toEqual(201)
  const ticketId = response.body.id
  // get the ticket
  const foundTicket = await request(app)
    .get(`/api/tickets/${ticketId}`)
    .send();
  expect(foundTicket.status).toEqual(200);
  expect(foundTicket.body.title).toEqual('My Test ticket');
  expect(foundTicket.body.price).toEqual(100);
})