import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

// Tell JEST to use FAKE wrapper (path to real file)
jest.mock('../../nats-wrapper');

const createNewTicekt = async (title:string, price:string, uid?:string) => {
  return await request(app)
    .post('/api/tickets')
    .set('Cookie', uid ? global.signup(uid): global.signup())
    .send({
      title: 'Hello World Ticket',
      price: '10.00'
    })
}


it('Returns a 404 if the provided ID does not exist', async () => {
  const cookie = global.signup()
  const id = new  mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookie)
    .send({
      title: 'Hello World Ticket',
      price: '10.00'
    }).expect(404)
});

it('Returns a 401 if the user is not authenticated', async () => {
  const id = new  mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/tickets/${id}`)
    .send({
      title: 'Hello World Ticket',
      price: '10.00'
    }).expect(401)
});

it('Returns a 401 if the user does not own the ticket', async () => {
  const newTicketResponse = await createNewTicekt('New Ticket Test', '10.00')
  expect(newTicketResponse.status).toEqual(201)
  const id = newTicketResponse.body.id;
  const cookieNewUser = global.signup('uid-123457')
  await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookieNewUser)
    .send({
      title: 'Hello World Ticket Update!',
      price: '20.00'
    }).expect(401)
});

it('Returns a 400 if the provide an invalid title or price', async () => {
  const cookie = global.signup()
  const newTicketResponse = await createNewTicekt('Testing Ticket', '15.00');
  expect(newTicketResponse.status).toEqual(201);
  const id = newTicketResponse.body.id;
  await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookie)
    .send({
      title: '',
      price: '20.00'
    }).expect(400);
    await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookie)
    .send({
      title: 'Hello World Ticket Update!',
      price: '-20.00'
    }).expect(400);
});

it('Returns a 200 if the user provide valid inputs and own the ticket', async () => {
  const cookie = global.signup()
  const newTicketResponse = await createNewTicekt('Testing Ticket', '15.00');
  expect(newTicketResponse.status).toEqual(201)
  const id = newTicketResponse.body.id;
  const response = await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookie)
    .send({
      title: 'Hello World Ticket Update!',
      price: '20.00'
    });
  expect(response.status).toEqual(200);
  expect(response.body.title).toEqual('Hello World Ticket Update!')
  expect(response.body.price).toEqual(20)
});