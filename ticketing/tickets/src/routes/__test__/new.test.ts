import request from 'supertest';
import { app } from '../../app';
import { Ticket } from '../../models/ticket';
import { natsWrapper } from '../../nats-wrapper';

// Tell JEST to use FAKE wrapper (path to real file)
jest.mock('../../nats-wrapper');

// This is a test-driven dev example
it('Has a route handler listening to /api/tickets for POST requests', async () => {
  const response = await request(app)
    .post('/api/tickets')
    .send({});
  expect(response.status).not.toEqual(404);
});

it('Can only be accessed if the user is signed in', async () => {
  const response = await request(app)
    .post('/api/tickets')
    .send({});
  // 401 is the not authorized exception error code
  expect(response.status).toEqual(401);
});

it('Returns an status other than 401 if the user is signed in', async () => {
  const cookie = await global.signup();
  const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({});
  expect(response.status).not.toEqual(401);
});

it('Returns an error if an invalid title is provided', async () => {
  const cookie = await global.signup();
  const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
      title: '',
      price: 10
    });
  expect(response.status).toEqual(400);
});

it('Returns an error if an invalid price is provided', async () => {
  const cookie = await global.signup();
  const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
      title: 'Testin Ticket',
      price: ''
    });
  expect(response.status).toEqual(400);
});

it('It creates a ticket with valid inputs', async () => {
  //See how many records we have
  let tickets = await Ticket.find({});
  expect(tickets.length).toEqual(0);
  const cookie = await global.signup();
  const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
      title: 'Ticke title',
      price: '10.00'
    });

  expect(response.status).toEqual(201);
  // Post a new ticket and see it increasing
  tickets = await Ticket.find({});
  expect(tickets.length).toEqual(1);
  expect(tickets[0].price).toEqual(10);
  expect(tickets[0].title).toEqual('Ticke title');

});

it('Publishes a new ticket event to fake NATSS', async () => {
  //See how many records we have
  let tickets = await Ticket.find({});
  expect(tickets.length).toEqual(0);
  const cookie = await global.signup();
  const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
      title: 'Ticke title',
      price: '10.00'
    });
  expect(response.status).toEqual(201);
  // By the moment this runs the fake function should have been called
  // Using the real import will return the JEST MOCK!!
  expect(natsWrapper.client.publish).toHaveBeenCalled();
})
