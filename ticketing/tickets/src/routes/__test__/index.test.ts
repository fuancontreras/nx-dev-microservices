import request from 'supertest';
import { app } from '../../app';

// Tell JEST to use FAKE wrapper (path to real file)
jest.mock('../../nats-wrapper');

const createTicket = async (title: string, price: string) => {
  const cookie = await global.signup();
  return await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
      title,
      price
    });
}

it('Retrieves all the tickets', async () => {
  await createTicket('My Ticket 1', '10.0');
  await createTicket('My Ticket 2', '20.0');
  const response = await request(app)
    .get('/api/tickets')
    .send({});
  expect(response.status).toEqual(200);
  expect(response.body.length).toEqual(2);
})