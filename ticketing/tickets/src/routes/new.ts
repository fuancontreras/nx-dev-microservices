import { requireAuth, validateRequest } from '@js-libs/common';
import express, { Request, Response } from 'express'
import { body } from 'express-validator';
import { TicketCreatedPublisher } from '../events/publishers/ticket-created-publisher';
import { Ticket } from '../models/ticket';
import { natsWrapper } from '../nats-wrapper';

// creating router
const router = express.Router();

// Declare route
// currentUser middleware is applied in app
router.post('/api/tickets', [
  requireAuth,
  body('title')
    .not()
    .isEmpty()
    .withMessage('Title is required'),
  body('price')
    .isFloat({ gt: 0 })
    .withMessage('Price must be greater than zero'),
  validateRequest
], async (req: Request, res: Response) => {
  const { title, price } = req.body;
  const ticket = Ticket.build({
    title,
    price,
    userId: req.currentUser!.id
  });
  await ticket.save();
  
  // Use the data from mongo to populate the service
  // natsWrapper SINGLETON called here!! natsWrapper is an instance!
  // Select await or not depending on what happens if it fails
  new TicketCreatedPublisher(natsWrapper.client).publish({
    id: ticket.id,
    title: ticket.title,
    price: ticket.price,
    userId: ticket.userId
  })
  
  res.status(201).send(ticket);
})

// Export router as named object property
export { router as newTicketRouter };