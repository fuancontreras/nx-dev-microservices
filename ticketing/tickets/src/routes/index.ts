import Router, { Request, Response } from 'express'
import { Ticket } from '../models/ticket';

// create route
const router = Router();

// Implement route
router.get('/api/tickets', async (req:Request, res:Response) => {
  const tickets = await Ticket.find({});
  res.status(200).send(tickets);
})

export { router as ticketsIndexRoute };