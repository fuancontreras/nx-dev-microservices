import { NotFoundError } from '@js-libs/common';
import express, { Request, Response } from 'express'
import { Ticket } from '../models/ticket';

// create the router
const router = express.Router();

// Declare route
router.get('/api/tickets/:id', async (req: Request, res: Response) => {
  const id = req.params.id;
  try {
    const ticket = await Ticket.findById(id);
    if (ticket) {
      res.status(200).send(ticket);
    } else {
      throw new NotFoundError();
    }
  } catch {
    throw new NotFoundError();
  }
});

export { router as showTicketRouter }