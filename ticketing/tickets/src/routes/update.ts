import { currentUser, NotFoundError, requireAuth, validateRequest, NotAuthorizedError } from '@js-libs/common';
import { Request, Response, Router } from 'express'
import { body } from 'express-validator';
import { TicketUpdatedPublisher } from '../events/publishers/ticekt-updated-publisher';
import { Ticket } from '../models/ticket';
import { natsWrapper } from '../nats-wrapper';

// declare router middleware
const router = Router();

// implement route
router.put('/api/tickets/:id', [
  requireAuth,
  currentUser,
  body('title')
    .notEmpty()
    .withMessage('Invalid Title'),
  body('price')
    .isFloat({ gt: 0 })
    .withMessage('Invalid Price'),
  validateRequest
], async (req: Request, res: Response) => {
  // as current user is there we should find it in the req
  const id = req.params.id
  const userId = req.currentUser!.id;

  const ticket = await Ticket.findById(id);
  if (!ticket) {
    throw new NotFoundError();
  }
  // Check auth
  if (ticket.userId !== userId) {
    throw new NotAuthorizedError();
  }
  // Perform mongoDb update
  ticket.set({...req.body});
  await ticket.save()
  // Notify
  // Select await or not depending on what happens if it fails
  new TicketUpdatedPublisher(natsWrapper.client).publish({
    id: ticket.id,
    title: ticket.title,
    price: ticket.price,
    userId: ticket.userId
  })
  res.status(200).send(ticket);
})

// export named route
export { router as updateTicketRoute }