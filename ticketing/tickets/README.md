# Tickets API Microservice
The tickets API handles all tickets related requirements in isoltaion (orders handle its own app). 
[Tickets Service Overview](https://www.udemy.com/course/microservices-with-node-js-and-react/learn/lecture/19123184#overview).

## Tickets Services Routes
![](https://snipboard.io/0hFHaj.jpg)
> When retreiving a ticket with specific ID the user will only have access to tickets created by the same UID which is related in the auth session.