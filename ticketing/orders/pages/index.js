import { authServiceAxiosFactory } from "../api/build-client";

const DefaultPage = ({ currentUser }) => {
  return currentUser ? <h1>You are signed in</h1> : <h1>You are NOT signed in</h1>
}

// NextJS server/client function (includes request)
DefaultPage.getInitialProps = async (context) => {
  const { data } = await authServiceAxiosFactory(context).get('/api/users/currentusers');
  return data;
};

export default DefaultPage;
