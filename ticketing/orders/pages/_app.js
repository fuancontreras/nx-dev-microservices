import 'bootstrap/dist/css/bootstrap.css';
import { authServiceAxiosFactory } from '../api/build-client';
import { Header}  from '../components/header';

// Main app wrapper
const AppComponent = ({ Component, pageProps, currentUser }) => {
  return <div>
    <Header currentUser={currentUser} />
    <Component {...pageProps}></Component>
  </div> 
};

// NextJS server/client function (includes request)
AppComponent.getInitialProps = async (context) => {
  // context for Factory method is in context.ctx
  const { data } = await authServiceAxiosFactory(context.ctx).get('/api/users/currentusers');
  
  let pageProps = {};
  // Invoke components  getInitialProps when defined
  if (context.Component.getInitialProps) {
    pageProps = await context.Component.getInitialProps(context.ctx);
  }
  // Pass both AppComponent and Component properties
  return { pageProps, ...data };
};

export default AppComponent;