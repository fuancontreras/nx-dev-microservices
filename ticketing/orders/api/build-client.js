import axios from 'axios';

export const authServiceAxiosFactory = ({ req }) => {
  // deteting server
  if (typeof window === 'undefined') {
    // create axios preconfigured instace
    return axios.create({
      baseURL: 'http://ingress-nginx-controller.ingress-nginx.svc.cluster.local',
      // Forwarding cookies
      headers: req.headers
    });
  } else {
    return axios.create({
      baseURL: '/'
    })
  }
}