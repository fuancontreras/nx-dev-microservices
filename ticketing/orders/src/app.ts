import express from "express";
require("express-async-errors");

import { json } from "body-parser";
import { currentUser, errorHandler, NotFoundError } from "@js-libs/common";
import Cookiesession from 'cookie-session';

// Import routes
import { indexOrderRouter } from "./routes/index";
import { deleteOrderRouter } from "./routes/delete";
import { newOrderRouter } from "./routes/new";
import { showOrderRouter } from "./routes/show";


// create App
const app = express();
// add body parser
app.use(json());
// Set trust certificate certificatge.
app.set('trust proxy', true);

// Add cookie manager middleware
app.use(
  Cookiesession({
    // Disabling encryption
    signed: false,
    // Require https (Supertest not supported)
    secure: process.env.NODE_ENV !== 'test',
  })
);

// As all routes are authenticated use the currentUser middleware
app.use(currentUser);

// Apply route (register route)
app.use(indexOrderRouter);
app.use(deleteOrderRouter);
app.use(newOrderRouter);
app.use(showOrderRouter);

/** Default route */
app.all("*", async () => {
  throw new NotFoundError();
});

// Applyerror handler
app.use(errorHandler);

// Exporting app to be used with SuperTest
export { app };