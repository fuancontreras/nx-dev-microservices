import nats, { Stan } from 'node-nats-streaming';

class NatsWrapper {
  // With ? allowing it to be undefined for a while
  private _client?: Stan;

  // getter
  get client() {
    if(!this._client) {
      throw new Error('Cannot access NATS client before connecting');
    }
    return this._client
  }

  // connection setup
  connect(clusterId: string, clientId: string, url: string) {
    this._client = nats.connect(clusterId, clientId, { url });
    return new Promise((resolve, reject) => {
      // Using the getter to read client
      this.client.on('connect', () => {
        console.log('Connected to NATSS');
        resolve(true);
      })
      // Using the getter to read client
      this.client.on('error', (err) => {
        console.error(err);
        reject(err);
      })
    })

  }
}

// Export an instance of the class
export const natsWrapper = new NatsWrapper();