import express, { Request, Response } from 'express'
import { requireAuth, validateRequest } from '@js-libs/common';
import { body } from 'express-validator';
import mongoose from 'mongoose';


// Creates the router
const router = express.Router()

// Route implementation
router.get('/api/orders',
  requireAuth,
  [
    body('ticketId')
      .not()
      .isEmpty()
      // validate mongoose id custom validator
      .custom((input: string) => mongoose.Types.ObjectId.isValid(input))
      .withMessage('Ticket ID (ticketId) must be provided'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    res.send({});
  }
);

export { router as indexOrderRouter };