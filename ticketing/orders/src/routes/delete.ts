import express, {Request, Response} from 'express'

// Creates the router
const router = express.Router()

// Route implementation
router.delete('/api/orders/:orderId', async (req: Request, res: Response) => {
  res.send({});
});

export { router as deleteOrderRouter };