import mongoose from 'mongoose';
import { OrderStatus } from '@js-libs/common'
import { TicketDoc } from './ticket'

// 1 Interface for attribtues
interface OrderAttrs {
  userId: string,
  status: OrderStatus,
  expiresAt: Date,
  ticket: TicketDoc
}


// 2 Interface for document
interface OrderDoc extends mongoose.Document {
  userId: string,
  status: OrderStatus,
  expiresAt: Date,
  ticket: TicketDoc
}

// 3 Interface for model
interface OrderModel extends mongoose.Model<OrderDoc> {
  build(attrs: OrderAttrs): OrderDoc
}

// 4 Define Schema
const orderSchema = new mongoose.Schema({
  // uses mongo types
  userId: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    // restrict to ENUM options
    enum: Object.values(OrderStatus),
    // Add a default value
    default: OrderStatus.Created
  },
  expiresAt: {
    // Using MongoDB Date type
    type: mongoose.Schema.Types.Date,
  },
  ticket: {
    // Type object ID
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Ticket'
  }
}, {
  toJSON: {
    // format returned objkect with .id instead of ._id
    transform(doc, ret) {
      ret.id = ret._id
      delete ret._id
    }
  }
})

// 5 Define Factory
orderSchema.statics.build = (attrs: OrderAttrs) => {
  return new Order(attrs);
};

// 6 Define the model constant
const Order = mongoose.model<OrderDoc, OrderModel>('Order', orderSchema);

// 7 Export Order
export { Order };