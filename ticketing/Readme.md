# Ticketing Microservices Application
Microservices based application
### Adding Secret via command
The app needs a secret to create web tokens. Use the following command to add the secret to the cluster.
```sh
kubectl create secret generic jwt-secret --from-literal=JWT_KEY=asdf
```
---
## Deploying app in different providers
Devspace is configured so you can deploy or develop using `K8s Docker For Mac`, `Minikube`, `Okteto`.

### Running with devspace locally Minikube
Get minikube IP
```
minikube ip
```
Add `/etc/hosts` entry for `ticketing.dev minikubeIP` 
Run `devspace` command `switch-minikube`
```sh
devspace run switch-minikube
```
> There is a bug with minikube ingress which doesn't expose `nginx-ingress-controller` service through port `80`. 
Once you enable minikube nginx addon, expose the port so the Server Side Rendering works as expected.
```
kubectl expose deployment ingress-nginx-controller --target-port=80 --type=ClusterIp -n kube-system
```
### Using local (Docker for Desktop)
Add `/etc/hosts` entry for `ticketing.dev 127.0.0.1`

Run `devspace` command `switch-local`
```sh
devspace run switch-local
```
### Using okteto

Install okteto client
https://okteto.com/docs/getting-started/installation/index.html
```
$ brew install okteto
devspace run switch-octeto
okteto namespace
```
Authenticate docker so it can publish images to okteto.
Use token as password: https://cloud.okteto.com/#/settings/setup
```
docker login registry.cloud.okteto.net
kubectl get serviceaccount default -o yaml
```
---
## Updating images for cloud deployment
Devspace skip images push by default so there is a command that pushes the images to the `:latest` tag.

### Updating images in docker registry
```
devspace run switch-local
devspace run publish-latest-images
```
### Updating images in Okteto registry
```
devspace run switch-okteto
devspace run publish-latest-images
```
---
# Custom Project Commands

## devspace run publish-latest-images
Builds and pushes `:latest` images to the current registry.

## devspace run commonjs-pub
Builds and publishes the `common-js` library and rebuilds all dependent images.