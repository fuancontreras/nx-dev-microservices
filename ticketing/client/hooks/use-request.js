import { useState } from 'react';
import axios from 'axios';

export const useRequest = ({ url, method, body, onSuccess=() => {} }) => {
  const [errors, setErrors] = useState(null);
  const doRequest = async () => {
    try {
      const response = await axios[method](url, body);
      setErrors(null);
      onSuccess(response.data);
    } catch (err) {
      setErrors(
        <div className="alert alert-danger" role="alert">
          <h4 className="alert-heading">Oops...</h4>
          <ul className="my-0">
            {err.response.data.errors.map((err, index) => (
              <li key={index}>{err.message}</li>
            ))}
          </ul>
        </div>
      )
    }
  }
  return [doRequest, errors]
};