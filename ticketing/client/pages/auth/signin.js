import { useState } from 'react';
import { useRequest } from '../../hooks/use-request';
import Router from 'next/router';

const SigninForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [doRequest, errors] = useRequest({
    url: '/api/users/signin',
    method: 'post',
    body: { email, password },
    onSuccess: () => Router.push('/')
  }
  );
  const onSubmit = async (event) => {
    event.preventDefault();
    await doRequest();
  }

  return <form onSubmit={onSubmit}>
    <h1>Sign in</h1>
    <div className="form-group">
      <label htmlFor="email">Email Address:</label>
      <input value={email} onChange={e => setEmail(e.target.value)}
        type="text" name="email" id="email" className="form-control" placeholder="Email address" aria-describedby="helpMail" />
      <small id="helpMail" className="text-muted">User e-mail address</small>
    </div>
    <div className="form-group">
      <label htmlFor="password">Password:</label>
      <input value={password} onChange={e => setPassword(e.target.value)} type="password" name="password" id="password" className="form-control" placeholder="Password" aria-describedby="helpPassword" />
      <small id="helpPassword" className="text-muted">User password</small>
    </div>
    {errors}
    <button className="btn btn-primary">Sign in</button>
  </form>
}

export default SigninForm;