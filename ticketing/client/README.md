## Client Microservice
This application contains the NextJs application that renders the UI.

## SSR and Authentication
Session persistence is handled by checking the JWT header existence. 
[More Info on SSR and authentication](https://www.udemy.com/course/microservices-with-node-js-and-react/learn/lecture/19119748#overview).

export const authServiceAxiosFactory = ({ req }) => {
The hybrid auth imlementation can be found in `/pages/_app.js` (which is the general page model) using the `authServiceAxiosFactory`.

> This is necessary because AJAX sends cookie headers automatically, however when the callback is performed from the server (on first SSR renders), then the client headers need to be forwarded into the server request (which also uses a different URL to access the services via kubernetes).

```ts
import axios from 'axios';

export const authServiceAxiosFactory = ({ req }) => {
  // deteting server
  if (typeof window === 'undefined') {
    // create axios preconfigured instace
    return axios.create({
      baseURL: 'http://ingress-nginx-controller.ingress-nginx.svc.cluster.local',
      // Forwarding cookies
      headers: req.headers
    });
  } else {
    return axios.create({
      baseURL: '/'
    })
  }
}
```