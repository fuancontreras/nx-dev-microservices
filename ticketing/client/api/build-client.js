import axios from 'axios';

export const authServiceAxiosFactory = ({ req }) => {
  // Detecting server exec
  if (typeof window === 'undefined') {
    // create axios preconfigured instace
    return axios.create({
      // Default: http://ingress-nginx-controller.ingress-nginx.svc.cluster.local
      baseURL: process.env.INGRESS_SVC_URL,
      // Forwarding cookies
      headers: req.headers
    });
  } else {
    return axios.create({
      baseURL: '/'
    })
  }
}