import nats from 'node-nats-streaming';
import { randomBytes } from 'crypto'
import { TicketCreatedListener } from './events/ticket-created-listener';
console.clear();

// create client addign useless
const stan = nats.connect('ticketing', randomBytes(4).toString('hex'), {
  url: 'http://localhost:4222',
});

// Handler for console close events
process.on('SIGINT', () => { stan.close() });
process.on('SIGTERM', () => { stan.close() });

// Instanciate and listen!
stan.on('connect', () => {
  // Set gracefull close event
  stan.on('close', () => {
    console.log('NATS connection closed!');
    process.exit();
  });
  const myListener = new TicketCreatedListener(stan);
  myListener.listen();
})
