import { PublisherBase } from "@js-libs/common";
import { Subjects } from "@js-libs/common";
import { TicketCreatedEvent } from "@js-libs/common";

export class TicketCreatedPublisher extends PublisherBase<TicketCreatedEvent> {
  subject: Subjects.TicketCreated = Subjects.TicketCreated
}