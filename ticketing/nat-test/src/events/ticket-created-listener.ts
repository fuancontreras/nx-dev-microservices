import { Message } from "node-nats-streaming";
import { Listener } from "@js-libs/common";
import { Subjects } from "@js-libs/common";
import {TicketCreatedEvent} from '@js-libs/common';

export class TicketCreatedListener extends Listener<TicketCreatedEvent> {
  // Channel name
  readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
  // Queue group name
  queueGroupName = 'payments-service';

  // On message abstract implmentation using enforced type from generic
  onMessage(data: TicketCreatedEvent['data'], msg: Message) {
    console.log('Event data!', data)
    // Acknowledge
    msg.ack();
  }
}