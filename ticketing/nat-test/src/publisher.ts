import nats from 'node-nats-streaming';
import { Subjects } from '@js-libs/common';
import { TicketCreatedPublisher } from './events/ticket-created-publisher';

console.clear();

// Create client (stan)
const stan = nats.connect('ticketing', 'abc', {
  url: 'http://localhost:4222'
});

// Listen for connect event
stan.on('connect', async () => {
  console.log('Publisher connected to NATS');
  // Instanciate the publisher
  const pub = new TicketCreatedPublisher(stan)
  const data = {
    id: '123',
    title: 'Concert',
    price: 20,
    userId: 'coco'
  };

  // publish data
  try {
    await pub.publish(data)  
  } catch(err) {
    console.error(err)
  }
})