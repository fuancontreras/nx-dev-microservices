# Helm Configuration
Make sure you have a good nginx-ingress version.
Run this if removing old one. (I had to reset k8s in Docker for Mac)
```
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission
```

Make sure you have helm 3 and nginx already configured.
```sh
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install my-release ingress-nginx/ingress-nginx
```