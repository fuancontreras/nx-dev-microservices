#!/bin/sh
echo "Building images for registry: $1"
echo ${REGISTRY_PREFIX}${REGISTRY}

# devspace cleanup images
devspace build --force-build --tag latest

docker push $1/commonjs
docker push $1/auth
docker push $1/client
docker push $1/orders
docker push $1/tickets