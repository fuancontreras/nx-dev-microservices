#!/bin/sh
set -e

# Authenticate if required
devspace enter -c commonjs -- sh scripts/authenticate-npm.sh

# Publish
devspace enter -c commonjs -- npm run pub
_now=$(date +"%m_%d_%Y")

# Updates package json definitions
devspace enter -c orders -- npm update --save --package-lock-only @js-libs/common
devspace enter -c tickets -- npm update --save --package-lock-only @js-libs/common
devspace enter -c auth -- npm update --save --package-lock-only @js-libs/common

# wait for sync
sleep 5

# Redeploy images via reload
echo $_now >> common/upgrade_log.txt