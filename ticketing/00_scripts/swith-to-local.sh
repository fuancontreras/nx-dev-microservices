#!/bin/sh
echo "Switching to Docker for Deskop K8s infraestructure"
devspace use context docker-desktop
kubectl create namespace ticketing
kubectl config set-context --current --namespace=ticketing 
devspace use profile default
devspace reset vars
echo "Switching to Docker for Deskop K8s DONE!"
