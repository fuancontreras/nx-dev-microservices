#!/bin/sh
echo "Switching to Minikube infraestructure"
devspace use context minikube
kubectl create namespace ticketing
kubectl config set-context --current --namespace=ticketing 
devspace use profile minikube
devspace reset vars
echo "Switching to Minikube DONE!"
