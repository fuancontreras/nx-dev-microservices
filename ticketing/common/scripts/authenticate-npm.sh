#!/bin/sh
set -e

cd /app

if test -f "/tmp/auth"; then
    echo "Already logged in (skipping auth)"
else
    npm login
    echo "Authenticated" >> /tmp/auth
fi